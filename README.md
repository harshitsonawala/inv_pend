# Linear Inverted Pendulum
This is the final assignment for MTE 4007: Robot Dynamics and Control, Department of Mechatronics Engineering, Manipal Institute of Technology

You need to use the alloted control algorithm to control the LIP model. 
Write the code for your controller in [control.py](https://gitlab.com/rdc-lab/inv_pend/blob/master/control/control.py)

## Instructions

In order to run this package, add it to your catkin workspace (you can look up [Lab 0](https://gitlab.com/rdc-lab/2018/tree/master/lab_1) to create your own catkin workspace). You can the use
the following commands, each in a new terminal window:

1. Start roscore
```bash
  $ roscore
```
2. Launch the model in Gazebo
```bash
  $ roslaunch inv_pend inv_pend.launch 
```
3. Run the controller
```bash
  $ rosrun inv_pend control.py 
```


